---
author: Kiara Jouhanneau | @kajou.design | kajou.design@gmail.com | ko-fi.com/kajoudesign   
title: Bristol  
project: paged.js sprint
book format: letter
book orientation: portrait
version: v1.0

Inspired by symphony and music scores

---
  
## → fonts/type designers :  
### Linux Libertine + Linux Biolinum + Linux Keyboard by [Philipp Poll](https://libertine-fonts.org)  
>*Libertine Fonts is a collection of libre multilingual fonts. Libertine Fonts are inspired by but not copying Arial, Times and other widely known an monotonous fonts. They provide many languages, symbols and small capitals to give a full set of professionnal and versatile libre fonts*  
### IA Writer Mono by [Information Architects Inc.](https://github.com/iaolo/iA-Fonts)  
>*iA Writer comes bundled with iA Writer for Mac and iOS.This is a modification of IBM's Plex font. The upstream project is [here](https://github.com/IBM/plex)*

## → the following classes :  
>> **Component Basis**  
component-body  
component-front  
component-title  
content-opener-image  
decoration  
paragraph  

>> **Component: Long**  
biography  
case-study  
feature   
long  

>> **Component: Shortboxes**  
note  
reminder  
short  
tip  
warning  

>> **Features: Closing**  
key-term  
key-terms  
references  
review-activity  
summary  

>> **Page: Chapter**  
chapter  
chapter-number  
focus-questions  
introduction  
learning-objectives  
outline  
outline-remake  

>> **Page: Part**  
part  
part-number  
outline  


>> **Page: TOC**  
ct  
toc   
toc-body  
toc-chapter  
toc-part  
name  

>> **Others**    
restart-numbering  
running-left  
running-right  
start-right  

